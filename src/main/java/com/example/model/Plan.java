package com.example.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

/**
 * @author chaithraNagraj on 03-04-2022
 */
@Data
@FieldDefaults(level = AccessLevel.PROTECTED)
public abstract class Plan {
    Double rate;
}
