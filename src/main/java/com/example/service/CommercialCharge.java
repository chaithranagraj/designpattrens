package com.example.service;

import com.example.model.Plan;
import org.springframework.stereotype.Component;

/**
 * @author chaithraNagraj on 03-04-2022
 */
@Component
public class CommercialCharge extends Plan {

    public Double getRate(){
        rate=6.9;
        return rate;
    }
}
