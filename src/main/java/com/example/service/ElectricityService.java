package com.example.service;

import com.example.model.Plan;
import org.springframework.stereotype.Service;

/**
 * @author chaithraNagraj on 03-04-2022
 */
@Service
public class ElectricityService {

    public Double calculateBill(Double units, Plan plan) {

        Double rate = plan.getRate();

        return rate * units;
    }
}
