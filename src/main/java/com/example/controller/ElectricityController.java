package com.example.controller;

import com.example.factory.GetFactory;
import com.example.model.Plan;
import com.example.service.ElectricityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author chaithraNagraj on 03-04-2022
 */
@RequestMapping("/api")
@RestController
public class ElectricityController {

    @Autowired
    GetFactory getFactory;

    @Autowired
    ElectricityService electricityService;

    @GetMapping
    public String getName() {
        String name = "chaithra";
        return name;
    }

    @GetMapping(value = "/getBill")
    public Double getBill(@RequestParam String type, @RequestParam Double units) {
        Plan plan = getFactory.getPlanType(type);
        return electricityService.calculateBill(units, plan);

    }
}
