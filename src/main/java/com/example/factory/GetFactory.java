package com.example.factory;

import com.example.model.Plan;
import com.example.service.CommercialCharge;
import com.example.service.DomasticCharge;
import com.example.service.InstituteCharge;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * @author chaithraNagraj on 03-04-2022
 */
@Component
@Slf4j
public class GetFactory {

    @Autowired
    CommercialCharge commercialCharge;
    @Autowired
    DomasticCharge domasticCharge;

    @Autowired
    InstituteCharge instituteCharge;

    public Plan getPlanType(String type){

        if(StringUtils.isEmpty(type)){
            log.info("type cannot be empty");
        }
        if(type.equals("DOMESTIC")){
            return domasticCharge;
        }
        if(type.equals("COMMERCIAL")){
            return commercialCharge;
        }
        if(type.equals("INSTITUTE")){
            return instituteCharge;
        }
        return null;

    }
}
